/*
Copyright (c) 2023 China Mobile Information Technology Co., Ltd
OpenGauss Operator is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
*/

package service

import (
	v13 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/klog/v2"
	customClient "openGauss-operator/internal/client"
	"openGauss-operator/internal/util"
	"path"
	"strings"
)

type LogCollectionInterface interface {
	ReconcileLogCollection() error
}

type LogCollection struct {
	K8sClient customClient.K8sClient
}

var _ LogCollectionInterface = &LogCollection{}

func NewLogCollection(client customClient.K8sClient) LogCollectionInterface {
	return &LogCollection{K8sClient: client}
}

func (r *LogCollection) ReconcileLogCollection() error {

	if err := r.newConfigMap(); err != nil {
		return err
	}

	daemonset, err := r.K8sClient.GetDaemonSet(util.LogCollectionNamespace, util.LogCollectionName)

	if err != nil {
		if errors.IsNotFound(err) {
			daemonset = newFluentdDaemonSet()
			if err := r.K8sClient.Create(daemonset); err != nil {
				return err
			}
		} else {
			klog.Errorf("Get log collection daemonset err %s", err)
		}
	}

	return nil
}

func (r *LogCollection) newConfigMap() error {

	fluentdConfigMap, err := r.K8sClient.GetConfigMap(util.LogCollectionCMName, util.LogCollectionNamespace)

	if err != nil {
		if errors.IsNotFound(err) {
			err, fluentdConfigMap = newFluentdConfigMap()
			if err != nil {
				return err
			}

			if err := r.K8sClient.Create(fluentdConfigMap); err != nil {
				klog.Errorf("Create configMap err %s", err)
				return err
			}
		} else {
			klog.Errorf("Get configMap err %s", err)
			return err
		}
	}

	return nil
}

func newFluentdConfigMap() (error, *v1.ConfigMap) {
	data := map[string]string{}
	var err error

	fileName := path.Join("customConfig", "configmap",
		"fluentd.conf")
	err, data[util.SubPathFluentd] = util.ReadFile(fileName)

	if err != nil {
		return err, nil
	}
	data[util.SubPathFluentd] = strings.Replace(data[util.SubPathFluentd], "brokers xxxx", "brokers "+util.KafkaBrokers, 1)
	return nil, &v1.ConfigMap{
		ObjectMeta: metaV1.ObjectMeta{
			Name:      util.LogCollectionCMName,
			Namespace: util.LogCollectionNamespace,
			Labels:    setDaemonsetLabel(),
		},
		Data: data,
	}
}

func setDaemonsetLabel() map[string]string {
	return map[string]string{
		util.DaemonsetTypeKey: util.FluentdDaemonset,
	}
}

func newFluentdDaemonSet() *v13.DaemonSet {
	var user int64 = 0
	var group int64 = 0

	var daemonset = &v13.DaemonSet{
		TypeMeta: metaV1.TypeMeta{
			APIVersion: util.LogCollectionApiVersion,
			Kind:       util.LogCollectionKind,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      util.LogCollectionName,
			Namespace: util.LogCollectionNamespace,
			Labels:    setDaemonsetLabel(),
		},
		Spec: v13.DaemonSetSpec{

			Selector: &metaV1.LabelSelector{
				MatchExpressions: []metaV1.LabelSelectorRequirement{
					{
						Key: util.DaemonsetTypeKey,
						Values: []string{
							util.FluentdDaemonset,
						},
						Operator: util.OperatorIn,
					},
				},
			},
			Template: v1.PodTemplateSpec{
				ObjectMeta: metaV1.ObjectMeta{
					Labels: setDaemonsetLabel(),
				},
				Spec: v1.PodSpec{
					Containers: []v1.Container{
						{
							Name:  util.ContainerNameFluentd,
							Image: util.FluentdImage,
							Resources: v1.ResourceRequirements{
								Limits: v1.ResourceList{
									"cpu":    resource.MustParse("4"),
									"memory": resource.MustParse("16Gi"),
								},
								Requests: v1.ResourceList{
									"cpu":    resource.MustParse("1"),
									"memory": resource.MustParse("1Gi"),
								},
							},
							VolumeMounts: []v1.VolumeMount{
								{
									Name:      util.VMName1,
									MountPath: util.FluentdConfMountPath,
									SubPath:   util.FluentdConfSubPath,
								},
								{
									Name:      util.VMName2,
									MountPath: util.MountPatGaussData,
								},
							},
						},
					},
					Affinity: &v1.Affinity{
						NodeAffinity: &v1.NodeAffinity{
							RequiredDuringSchedulingIgnoredDuringExecution: &v1.NodeSelector{
								NodeSelectorTerms: []v1.NodeSelectorTerm{
									{
										MatchExpressions: []v1.NodeSelectorRequirement{
											{
												Key: util.NodeTypeKey,
												Values: []string{
													util.NodeTypeValue,
												},
												Operator: "In",
											},
										},
									},
								},
							},
						},
					},
					Volumes: []v1.Volume{
						{
							Name: util.VMName1,
							VolumeSource: v1.VolumeSource{
								ConfigMap: &v1.ConfigMapVolumeSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: util.VMName3,
									},
								},
							},
						},
						{
							Name: util.VMName2,
							VolumeSource: v1.VolumeSource{
								HostPath: &v1.HostPathVolumeSource{
									Path: util.HostPathPrefix,
								},
							},
						},
					},
					ImagePullSecrets: []v1.LocalObjectReference{
						{
							Name: util.ImagePullSecrets,
						},
					},
					SecurityContext: &v1.PodSecurityContext{
						RunAsUser:  &user,
						RunAsGroup: &group,
						FSGroup:    &group,
					},
				},
			},
		},
	}
	return daemonset
}
