/*
Copyright (c) 2023 China Mobile Information Technology Co., Ltd
OpenGauss Operator is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
*/

package util

// 集群部署架构，OpenGaussClusterSpec.Type的值
const (
	// 拆分，cms和db分开部署在不同的Pod
	ArchitectureSplit = "split"
	// 共同，cm和db在一个Pod
	ArchitectureMerge = "merge"
)

// 网络架构
const (
	NetworkTypeNodePort = "overlayNodePort"
)

// storage
const (
	StorageTypeLocal = "local"
	StorageTypeShare = "share"
	StorageTypeMix   = "mix"
	HostPathPrefix   = "/data/cmdb/"
)

// label/annotation
const (
	BaseNameKey          = "cmdb.cmos.cn/base"
	BaseNameValue        = "cmdb"
	OgcNameKey           = "cmdb.cmos.cn/ogcName"
	PodNameKey           = "cmdb.cmos.cn/podName"
	PodTypeKey           = "cmdb.cmos.cn/podType"
	StorageTypeKey       = "cmdb.cmos.cn/storageType"
	DaemonsetTypeKey     = "cmdb.cmos.cn/daemonsetType"
	FluentdDaemonset     = "opengauss-fluentd"
	ContainerNameFluentd = "fluentd"
	CalicoFixedIpKey     = "cni.projectcalico.org/ipAddrs"
	HostnameKey          = "kubernetes.io/hostname"
	OperatorIn           = "In"
)

// pod type
const (
	PodTypeDB       = "gauss-db"
	PodTypeCMS      = "split-cms"
	PodTypeExporter = "gauss-exporter"
)

// container
const (
	ContainerNameGaussDb       = "gauss-db"
	ContainerNameGaussCms      = "split-cms"
	ContainerNameGaussExporter = "gauss-exporter"
	VolumeGaussData            = "gauss-data"
	MountPatGaussData          = "/opengauss/cluster"
	BusyBox                    = "k8s-deploy/busybox:latest"
)

// log collection
const (
	FluentdImage            = "k8s-deploy/fluentd:edge-debian"
	LogCollectionNamespace  = "kube-system"
	LogCollectionName       = "cmdb-fluentd-ds"
	LogCollectionCMName     = "cmdb-fluentd-cm"
	LogCollectionApiVersion = "apps/v1"
	LogCollectionKind       = "DaemonSet"
	VMName1                 = "fluentd-conf"
	FluentdConfMountPath    = "/fluentd/etc/fluent.conf"
	FluentdConfSubPath      = "fluent.conf"
	VMName2                 = "cmdb-log"
	NodeTypeKey             = "nodetype"
	NodeTypeValue           = "cmdb"
	VMName3                 = "cmdb-fluentd-cm"
	ImagePullSecrets        = "image-pull-secret-opengauss"
	SubPathFluentd          = "fluent.conf"
	KafkaBrokers            = ""
)

const (
	GsPassword                 = "test@123"
	CmsPodNum                  = 3
	OpenGaussClusterKind       = "OpenGaussCluster"
	OpenGaussClusterBrKind     = "OpenGaussBackupRecovery"
	OpenGaussClusterApiVersion = "gaussdb.gaussdb.middleware.cmos.cn/v1"
)

// gauss exporter
const (
	ContainerPortExporter  = 9187
	ExporterDataSourceName = "DATA_SOURCE_NAME"
	ExporterUsername       = "monitor"
	ExporterPassword       = "Test_123"
)

// br
const (
	MountPathBrData         = "/backup/gauss"
	CronBackupContainerName = "cron-backup"
	EnvNameAddress          = "address"
	EnvNameMode             = "mode"
	EnvNameBackupFile       = "backupFile"
	EnvNameBrUsername       = "BrUsername"
	EnvNameBrPassword       = "BrPassword"
	BrUsername              = "rep1"
	BrPassword              = "sdfg.1314"
	EnvNameTransferUsername = "TransferUsername"
	EnvNameTransferPassword = "TransferPassword"
	TransferUsername        = "root"
	TransferPassword        = "test#123"
	EnvNameInstanceIp       = "instanceIp"
	TTLSecondsAfterFinished = 120
)

// backup
const (
	BackupStageStopJob        = "StopBackupJob"
	BackupStageStopJobEnd     = "StopJobEnd"
	BackupStageCreate         = "Create"
	BackupStageRunning        = "BackupRunning"
	BackupStageSucceed        = "BackupSucceed"
	BackupStageFailed         = "BackupFailed"
	ManualBackupContainerName = "manual-backup"
	TempBackupPodSuffix       = "-0"
	BackupRequestCpu          = "1"
	BackupRequestMem          = "1Gi"
	BackupLimitCpu            = "4"
	BackupLimitMem            = "8Gi"
	BackupRestartPolicyNever  = "Never"
	BackoffLimit              = 3
	PodLogTailLine            = 20
	BackupSucceedLogSign      = "completed"
	JobPodCompletedStatus     = "Succeeded"
)

// recovery
const (
	ManualRecoveryContainerName = "manual-recovery"
	RecoveryRequestCpu          = "1"
	RecoveryRequestMem          = "1Gi"
	RecoveryLimitCpu            = "4"
	RecoveryLimitMem            = "8Gi"
	RecoveryStageCloneDn        = "Create"
	RecoveryStageCreateJob      = "CreateRecoveryJob"
	RecoveryStageReadLog        = "ReadRecoveryLog"
	RecoveryStageStartDn        = "StartDn"
	RecoveryStageJudgeDnStatus  = "JudgeDnStatus"
	RecoveryStageSucceed        = "RecoverySucceed"
	RecoveryStageFailed         = "RecoveryFailed"
	RecoveryStageStopJob        = "StopRecoveryJob"
	RecoveryStageStopJobEnd     = "StopRecoveryJobEnd"
	RecoverySucceedLogSign      = "completed"
)

// delete data
const (
	FinalizerName        = "opengauss.finalizers.io"
	LabelOwnerCluster    = "OwnerCluster"
	LabelJobName         = "job-name"
	LabelMiddlewareKey   = "middleware"
	LabelMiddlewareValue = "opengauss"
)

// standby reconstruct
const (
	SrStageCreate            = "create"
	SrStageRunning           = "running"
	SrStageSucceed           = "succeed"
	SrStageFailed            = "failed"
	SrAnnotationStartTimeKey = "StandbyReconstructStartTime"
	TimeFormat               = "2006-01-02 15:04:05"
)

// Datanode State
const (
	DnStateNormal = "Normal"
)

// instance migration
const (
	ImStageCreate            = "create"
	ImStageRunning           = "running"
	ImStageSucceed           = "succeed"
	ImStageFailed            = "failed"
	ImAnnotationStartTimeKey = "StandbyReconstructStartTime"
)

// switchover state
const (
	SOBegin    = "begin"
	SOComplete = "complete"
	SOFailed   = "failed"
)

const (
	ClusterPhaseNone        = ""
	ClusterPhaseCreating    = "Creating"
	ClusterPhaseRunning     = "Running"
	ClusterPhaseReconciling = "Reconciling"
	ClusterPhaseUpdating    = "Updating"

	ClusterResourceChange = "resourceChange"
	ClusterInstanceChage  = "instanceChange"
	ClusterDrainPod       = "drainPod"
)
const (
	OpenGaussRunningState = "Normal"
)

const (
	CmsSuffixString = "-cms"
	DbSuffixString  = "-db"
)
