/*
Copyright (c) 2023 China Mobile Information Technology Co., Ltd
OpenGauss Operator is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
*/

package v1

import (
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// OpenGaussBackupRecoverySpec defines the desired state of OpenGaussBackupRecovery
type OpenGaussBackupRecoverySpec struct {
	Transfer   Transfer       `json:"transfer,omitempty"`
	CronBackup CronBackupData `json:"cronBackup,omitempty"`
	Backups    []BrData       `json:"backups,omitempty"`
	Recoveries []BrData       `json:"recoveries,omitempty"`
	Image      string         `json:"image,omitempty"`
}

type BrData struct {
	PodName    string `json:"podName,omitempty"`
	BackupFile string `json:"backupFile,omitempty"`
	JobName    string `json:"jobName,omitempty"`
	Status     string `json:"status,omitempty"`
}

type CronBackupData struct {
	Time    string `json:"time,omitempty"`
	PodName string `json:"podName,omitempty"`
}

// OpenGaussBackupRecoveryStatus defines the observed state of OpenGaussBackupRecovery
type OpenGaussBackupRecoveryStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// OpenGaussBackupRecovery is the Schema for the opengaussbackuprecoveries API
type OpenGaussBackupRecovery struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`

	Spec   OpenGaussBackupRecoverySpec   `json:"spec,omitempty"`
	Status OpenGaussBackupRecoveryStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// OpenGaussBackupRecoveryList contains a list of OpenGaussBackupRecovery
type OpenGaussBackupRecoveryList struct {
	metaV1.TypeMeta `json:",inline"`
	metaV1.ListMeta `json:"metadata,omitempty"`
	Items           []OpenGaussBackupRecovery `json:"items"`
}

func init() {
	SchemeBuilder.Register(&OpenGaussBackupRecovery{}, &OpenGaussBackupRecoveryList{})
}
